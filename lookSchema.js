const { checkSchema } = require('express-validator');

exports.lookSchema = checkSchema({
    from: {
        trim: true,
        isAlpha: {
            errorMessage: 'Alpha characters only',
        },
        isLength: {
            errorMessage: 'Airport codes must be 3 characters',
            options: { min: 3, max: 3 }
        }
    },
    to: {
        trim: true,
        isAlpha: {
            errorMessage: 'Alpha characters only',
        },
        isLength: {
            errorMessage: 'Airport codes must be 3 characters',
            options: { min: 3, max: 3 }
        }
    },
    timestamp: {
        isISO8601: true
    },
    adults: {
        isInt: {
            options: {
                min: 1,
                max: 9
            }
        }
    },
    children: {
        isInt: {
            options: {
                min: 0,
                max: 8
            }
        }
    }
})