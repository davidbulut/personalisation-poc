const express = require("express");
const mongo = require("mongodb").MongoClient;
const { validationResult } = require('express-validator');
const cors = require('cors');
const { lookSchema } = require("./lookSchema");
const app = express();

app.use(express.json());
app.use(cors());

//Connect to the specified mongoDB account
mongo.connect('mongodb+srv://davidbulut:mongoDbPersonalisation9686!@cluster0.m7xcl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(client => {
    console.log('Connected to Database');
    const db = client.db('recently-looked-booked');
    const lookedCollection = db.collection('looked');
    const bookedCollection = db.collection('booked');

    app.listen(3000, function() {
        console.log('listening on 3000')
    });

    app.post("/look", lookSchema, (req, res) => {
        const validationErr = validationResult(req);
        if (!validationErr.isEmpty()) {
            console.log(validationErr);
        } else {
            const from = req.body.from;
            const to = req.body.to;
            const timestamp = new Date(req.body.timestamp);
            const adults = req.body.adults;
            const children = req.body.children;
            lookedCollection.insertOne({ from, to, timestamp, adults, children }, (err, result) => {
                if (err) {
                    console.error(err)
                    res.status(500).json({ err: err })
                    return
                }
                console.log(result)
                res.status(200).json({ ok: true })
            });
        }
    });

    //Endpoint to GET data from mongoDB based on query parameters a user has searched for
    app.get("/looked/:from/:to/:days/:minAdults/:minChildren", lookSchema, (req, res) => {
        console.log(req.params);
        let days = parseInt(req.params.days) || 7;
        let fromAirport = req.params.from === "ANY" ? { "$exists": 1 } : req.params.from;
        let toAirport = req.params.to === "ANY" ? { "$exists": 1 } : req.params.to;
        let queryDate = new Date();
        queryDate.setDate(queryDate.getDate() - days);
        let minAdults = parseInt(req.params.minAdults) || 0;
        let minChildren = parseInt(req.params.minChildren) || 0;
        
        //Find data from dB based on query parameters
        lookedCollection.find({
            "from": fromAirport,
            "to": toAirport,
            "timestamp": {
                $gte: queryDate
            },
            "adults": {
                $gte: minAdults
            },
            "children": {
                $gte: minChildren
            }
        }).toArray((err, items) => {
            if (err) {
                console.error(err)
                res.status(500).json({ err: err })
                return
            }
            res.status(200).json({ lookedCollection: items })
        })
    });
    app.post("/book", (req, res) => {
        /* */
    });
    app.get("/booked", (req, res) => {
        /* */
    });

});